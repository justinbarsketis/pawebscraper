from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
import os
import shutil
import time
import csv
import string
import time
import os.path
from os import path
import datetime
import glob
# Web Crawler
# Only run this script at night so the server doesn't slow down and you affect normal use.
# script takes about 12 hours for every 400 clients. You can optimize this greatly by decreasing sleeps, but this increases the chance of errors
# you can run this headless if not downloading documents and is much easier.
# clientNumbers.csv is a list of policy IDs you want to crawl, one per row
# Get these from exporting your commission report for the last 13 months and getting rid of any duplicates
# output.csv will contain all the clients infromation
# report.pdf will show your portfolio information
# each client folder will contain their certificate of coverage, claim documents, and payment receipts
# File STRUCTURE
# webscraper/ root directory
# webscraper/output/ - all client documents
### webscraper/references/ - clientList and driver
# webscraper/references/chrome.exe - web browser chrome driver
# webscraper/references/clientNumbers.csv - master client list, one policyId per row
# errors.csv - policyIds that couldnt get information from, I use this as the client list the second run, many of these are cancelled policies
# errors_missing_certificates.txt policies that did not have certificates
# errors_missing_claims.txt policies that did had missing claims documents after first run
# to run script I use python browser.py >> output_background.txt
# this will output all the error logs to a folder if you do not want to see them live

# Main Methods at bottom called
# get_all_info('references/clientNumbers.csv', 3, 8) -> Will scrape all the data from list
# check_all_documents(3, 20) -> Will double check each user folder against website to ensure all claim documents and certificate of coverages downloaded correctly

# Configuration Variables
username = "USERNAME"  # lyncpay username
password = "PASSWORD"  # lyncpay password
# wherever the scrip is located on your computer, title folder webscraper
projectOutput = r"C:\######\webscraper\output"
# path to chromedriver.exe
chromeDriver = "/mnt/c/#######/webscraper/references/chromedriver.exe"
average_wait = 3  # how long to wait between actions
maximum_wati = 20  # how long to wait for elements to load before skipping


def getPolicyInfo(driver, policyNumber, delay, max_delay):
    loadPolicyPage(policyNumber, driver, delay, max_delay)
    clientsTotal = getClientInfo(policyNumber, driver, delay, max_delay)
    # firstName, middleName, lastName, address, phone1, phone2, fax, email, birthDate, memberId, country, status1, age, sex, planNumber, planOption, effectiveDate, firstEffective, servicingProducer, producerEmail, producerPhone, status2, frequency, nextDueDate, deductible
    print("Policy Info Scraped")
    # USE DICTIONARY
    # convert to string, remove commas and non ascii characters
    for x in range(len(clientsTotal)):
        word = clientsTotal[x]
        # word = str(word.encode('utf-8', 'ignore'))
        word = remove_non_ascii(word)
        word = word.replace(',', '')
        clientsTotal[x] = word
    time.sleep(1)
    try:
        get_certificate_of_coverage(clientsTotal[9], driver, delay, max_delay)
        print("Certificate of Coverage Downloaded, Claims")
    except Exception as e:
        print(e)
        print("EOB Error " + clientsTotal[9])
    time.sleep(1)
    pathClaims = create_user_folder(clientsTotal)
    time.sleep(1)
    # Get Claim Documents
    try:
        eob_pagination(policyNumber,
                       pathClaims, driver, delay, max_delay)
        print("EOB Scrapped Succesfully")
    except Exception as e:
        print(e)

    print(str(clientsTotal))
    return (clientsTotal)


def check_all_documents(delay, max_delay):
    driver = loginNewSession(delay, max_delay)

    line_count = 0
    policyIdList = next(os.walk('./output'))[1]

    for policyNumber in policyIdList:
        print("Checking Documents: " + policyNumber)
        try:
            loadPolicyPage(policyNumber, driver, delay, max_delay)
        except Exception as e:
            f = open("errors_checker.txt", "a")
            f.write(str(e))
            f.close()
            continue
        driver.find_element_by_link_text("Claims").click()
        time.sleep(delay)

        certificatePath = "output/" + policyNumber + "/" + \
            policyNumber + "_Certificate Of Coverage.pdf"
        has_certificate = path.exists(certificatePath)
        if not has_certificate:
            print("Missing Certificate")
            f = open("errors_missing_certificates.txt", "a")
            f.write(str(policyNumber) + "\n")
            f.close()
        claimsTotal = get_claim_total(driver)
        pathClaims = "output/" + policyNumber + "/claims"
        existingClaims = len(glob.glob1(pathClaims, "*.pdf"))
        if (existingClaims < claimsTotal):
            print("Missing Claims, Total: " + str(claimsTotal) +
                  " - In Folders: " + str(existingClaims))
            f = open("errors_missing_claims.txt", "a")
            f.write(str(policyNumber) + "\n")
            f.close()
            try:
                eob_pagination(policyNumber, pathClaims,
                               driver, delay, max_delay)
                print("EOB Scrapped Succesfully")
            except Exception as e:
                print("Failed EOB Download")
                print(e)
        line_count = line_count + 1
        if (line_count % 15 == 0):
            driver.quit()
            driver = loginNewSession(delay, max_delay)


def get_claim_total(driver):
    claimsTextClaims = driver.find_element_by_xpath(
        '//*[@id="pagingAreaClaims"]/div/div[2]/div').text
    if "No Records Found" in claimsTextClaims:
        return 0

    totalClaims = driver.find_element_by_xpath(
        '//*[@id="pagingAreaClaims"]/div/div[3]/div').text
    wordList = totalClaims.split()
    return int(wordList[-1])


def check_all_claims(policyNumber, pathClaims, driver, delay, max_delay):
    loadPolicyPage(policyNumber, driver, delay, max_delay)

    totalClaims = get_claim_total(driver)
    with open(pathClaims + "/claims_total.csv", mode='a') as csv_outfile:
        csv_writer = csv.writer(csv_outfile, delimiter=',')
        csv_writer.writerow([totalClaims])


def loadPolicyPage(policyNumber, driver, delay, max_delay):
    print("Loading Policy Page")
    driver.get(
        'https://lyncone.lyncpay.com/Pages/IHA/IndividualPolicies.aspx?NavLogWidgetName=TopMenu')
    try:
        WebDriverWait(driver, max_delay).until(
            EC.presence_of_element_located((By.ID, 'gridParticipantsList')))
    except Exception as e:
        print(str(e))
        print("Element gridParticipantsList never loaded" + str(policyNumber))
        f = open("errors.txt", "a")
        f.write(str(e))
        f.close()
        raise NameError('Element gridParticipantsList never loaded')
    print("Policy Page Loaded, Trying Input Search")
    time.sleep(1)
#    print(driver.page_source.encode("utf-8"))
    memberSearchMainContainer = driver.find_element_by_id(
        "gridParticipantsList")
    memberSearchContainer = memberSearchMainContainer.find_elements_by_class_name(
        "hdrcell")[9].find_element_by_tag_name("input")  # Input Policy ID in Search Field
    memberSearchContainer.send_keys(policyNumber)
    time.sleep(delay)
    print("Input Search Done, Trying to Load Policy Info")
    memberSearchContainer2 = memberSearchMainContainer.find_element_by_class_name(
        "ev_dhx_skyblue").find_elements_by_tag_name("td")[0]  # Wait for form to load and then click the first row to load client info
    memberSearchContainer2.click()
    time.sleep(1)
    try:
        WebDriverWait(driver, max_delay).until(
            EC.presence_of_element_located((By.ID, 'pOv_planInformation')))
    except Exception as e:
        print(str(e))
        print("Element pOv_planInformation never loaded" + str(policyNumber))
        with open("errors.csv", mode='a') as csv_outfile:
            csv_writer = csv.writer(csv_outfile, delimiter=',')
            csv_writer.writerow([str(policyNumber)])
        raise NameError('Element pOv_planInformation never loaded')
    time.sleep(delay+delay)
    print("Policy Info Loaded")


def getClientInfo(policyNumber, driver, delay, max_delay):
    rows = driver.find_element_by_id(
        "userInfo_grid").find_elements_by_tag_name("td")  # Take out all client information and put it into an array. Note there are two different containers, and every other element is client information
    rows2 = driver.find_element_by_id(
        "pOv_planInformation").find_elements_by_tag_name("td")
    return [rows[2].text, rows[4].text, rows[6].text, rows[8].text, rows[10].text, rows[12].text, rows[14].text, rows[16].text, rows[18].text, rows[20].text, rows[22].text, rows[24].text, rows[26].text,
            rows[28].text, rows2[3].text, rows2[5].text, rows2[7].text, rows2[9].text, rows2[11].text, rows2[13].text, rows2[15].text, rows2[17].text, rows2[19].text, rows2[21].text, rows2[23].text, rows2[25].text]


def get_certificate_of_coverage(policyId, driver, delay, max_delay):
    # Get Certificate of Coverage and create folder structure
    driver.find_element_by_class_name('icon23').click()
    actions = ActionChains(driver)
    actions.move_to_element_with_offset(driver.find_element_by_link_text(
        "Certificate of Coverage"), 5, 5).pause(1).click().perform()

    x = 0
    path = str(policyId) + "_Certificate Of Coverage.pdf"
    while not os.path.isfile(path):
        time.sleep(1)
        x = x + 1
        if (x == max_delay):
            break

    time.sleep(delay)


def eob_pagination(policyNumber, pathClaims, driver, delay, max_delay):
    driver.find_element_by_link_text(
        "Claims").click()
    time.sleep(delay)

    rows = driver.find_element_by_id("pagingAreaClaims").find_element_by_class_name(
        "dhx_pline_skyblue").find_elements_by_class_name("dhx_page_skyblue")
    print("Pagination Iterations: " + str(len(rows)))
    pages = len(rows)
    page = 0
    if pages == 1:
        page = -1  # let recursion know not to do anything
    # recursive function for pagination
    get_eob_documents(policyNumber, pathClaims,
                      driver, delay, max_delay, page)


def get_eob_documents(policyNumber, pathClaims, driver, delay, max_delay, page):
    WebDriverWait(driver, max_delay).until(
        EC.presence_of_element_located((By.ID, 'po_ClaimsInfo')))
    rows = driver.find_element_by_id("po_ClaimsInfo").find_element_by_class_name(
        "objbox").find_elements_by_tag_name("tr")
    print("Claims Page: " + str(page))
    if len(rows) == 0:
        return
    line_number = 0
    for x in rows:
        if line_number == 0:
            print("Break")
            line_number = line_number + 1
            continue
        claimInfo = x.find_elements_by_tag_name("td")

        name = remove_non_ascii(claimInfo[1].text)
        claimNumber = remove_non_ascii(claimInfo[0].text)
        dateSubmitted = remove_non_ascii(claimInfo[3].text)
        provider = remove_non_ascii(claimInfo[4].text)

        # check if claim already exist
        existing_claims_files = glob.glob(
            "output/" + policyNumber + "/claims" + "/*.pdf")

        if not any(claimNumber in s for s in existing_claims_files):
            with open(pathClaims + "/claims.csv", mode='a') as csv_outfile:
                csv_writer = csv.writer(csv_outfile, delimiter=',')
                csv_writer.writerow(
                    [claimNumber, name, claimInfo[2].text, dateSubmitted, provider, claimInfo[5].text])

            claimInfo[6].find_element_by_tag_name("img").click()
            x = 0
            while not os.path.isfile("output/EOB.pdf"):
                time.sleep(1)
                x = x + 1
                if (x == delay + 5):
                    break
            time.sleep(delay+1)  # download file

            if os.path.isfile("output/EOB.pdf"):
                shutil.move("output/" + "EOB.pdf", pathClaims + "/" + name + "_" +
                            dateSubmitted + "_" + claimNumber + "_" + provider + ".pdf")
                time.sleep(delay)
    if page < 0:
        print("No Pagination")
        return  # no recursion needed

    # Get Elements again to prevent stagnation of variables
    rows = driver.find_element_by_id("pagingAreaClaims").find_element_by_class_name(
        "dhx_pline_skyblue").find_elements_by_class_name("dhx_page_skyblue")
    page = page + 1
    rows[page].click()
    time.sleep(1)
    if page >= 10:
        print("Next Page Recursion")
        get_eob_documents(policyNumber, pathClaims,
                          driver, delay, max_delay, 1)
        return
    if page >= len(rows):
        return  # all rows done
    time.sleep(delay)
    get_eob_documents(policyNumber, pathClaims,
                      driver, delay, max_delay, page)


def remove_non_ascii(text):
    printable = set(string.printable)
    return (''.join(filter(lambda x: x in printable, text)))


def create_user_folder(clientInfo):
    policyId = clientInfo[9]
    name = clientInfo[2] + "_" + clientInfo[0]

    # Create client directory and move certificate of coverage into it
    path = "output/" + policyId
    pathClaims = path + "/claims"
    os.mkdir(path)
    os.mkdir(pathClaims)
    certificate = policyId + "_Certificate Of Coverage.pdf"
    shutil.move("output/" + certificate, path + "/" + certificate)

    with open(path + "/" + policyId + "_" + name + ".csv", mode='a') as csv_outfile:
        csv_writer = csv.writer(csv_outfile, delimiter=',')
        csv_writer.writerow(clientInfo)
    return pathClaims


def loginNewSession(delay, max_delay):
    # Start the Script with an incognito browser
    print("Initiating Login")
    options = webdriver.ChromeOptions()
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--incognito')
    # options.add_argument('--headless')
    options.add_argument("--window-size=1920x1080")
    options.add_argument("--disable-notifications")
    options.add_argument('--no-sandbox')
    options.add_argument('--verbose')
    prefs = {
        'download.default_directory': projectOutput,
        'profile.default_content_setting_values.automatic_downloads': 1}
    options.add_experimental_option('prefs', prefs)
    driver = webdriver.Chrome(chromeDriver, chrome_options=options)
    # function to handle setting up headless download

    driver.get("https://lyncone.lyncpay.com/pages/public/login.aspx")
    try:
        WebDriverWait(driver, max_delay).until(
            EC.presence_of_element_located((By.ID, 'cphMain_login_UserName')))
    except Exception as e:
        print(str(e))
        print("Element cphMain_login_UserName never loaded")
        f = open("errors.txt", "a")
        f.write(str(e))
        f.close()
    time.sleep(delay)
    print("Login Screen Loaded, attempting login")
    # Wait for slow lyncpay to load
    driver.find_element_by_id("cphMain_login_UserName").send_keys(username)
    driver.find_element_by_id(
        "cphMain_login_Password").send_keys(password)
    driver.find_element_by_id("cphMain_login_LoginButton").click()
    # Login
    try:
        WebDriverWait(driver, max_delay).until(
            EC.presence_of_element_located((By.ID, 'myAccountTabs')))
    except Exception as e:
        print(str(e))
        print("Element myAccountTabs never loaded")
        return driver
    print("Login Success")
    return driver


def get_all_info(clientNumbers, delay, max_delay):
    with open(clientNumbers) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        currentDriver = loginNewSession(delay, max_delay)
        for row in csv_reader:
            # tic = time.perf_counter()
            try:
                print("Attempting client information")
                # print("TimeElapsed: " + str(time.perf_counter() - tic))
                # get the clients information from their policy number
                clientInformation = getPolicyInfo(
                    currentDriver, row[0], delay, max_delay)
                # if there were no errors, write the clients information to the output file
                with open("output/output.csv", mode='a') as csv_outfile:
                    csv_writer = csv.writer(csv_outfile, delimiter=',')
                    csv_writer.writerow(clientInformation)
            except Exception as e:
                print(str(e))

            # print("TimeElapsed: " + str(time.perf_counter() - tic))
            print("Client #" + str(line_count))
            # print("TimeElapsed Before Login Check: " +     str(time.perf_counter() - tic))
            line_count = line_count + 1
            if (line_count % 15 == 0):
                currentDriver.quit()
                currentDriver = loginNewSession(delay, max_delay)
            # Login again after 15 sessions


# Get all clients info from client number list
get_all_info('references/clientNumbers.csv', 3, 8)

# Double checks each client folder to ensure there are no missing certificates or claims docs
check_all_documents(3, 20)
