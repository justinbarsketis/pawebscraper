This was a personal side project I created to help me gather our client's information from an insurers portal to import the data into a new CRM we are using.
This project uses Google's Selenium to replicate desired actions in Google Chrome.
If you do not know much about python please be careful with this script.
If you would like to contract out help reach me at team@expatinsurance.com
